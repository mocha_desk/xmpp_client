'use strict'

// const {Socket} = require('net')
const Connection = require('xmpp/connection')
const {Parser} = require('xmpp/xml')
const url = require('url')

const socks = require('socks')
const { SocksClient } = require('socks')

let Socket = require('net').Socket

const NS_STREAM = 'http://etherx.jabber.org/streams'

/* References
 * Extensible Messaging and Presence Protocol (XMPP): Core http://xmpp.org/rfcs/rfc6120.html
*/

class ConnectionTCP extends Connection {
  socketParameters(service) {
    const {port, hostname, protocol} = url.parse(service)

    const options = {
      proxy: {
        host: '10.61.76.58', // ipv4 or ipv6 or hostname
        port: 9222,
        type: 5 // Proxy version (4 or 5)
      },

      command: 'connect', // SOCKS command (createConnection factory function only supports the connect command)

      destination: {
        host: hostname, // github.com (hostname lookups are supported with SOCKS v4a and 5)
        port: port
      }
    }

    try {
      SocksClient.createConnection(options, (err, info) => {
        if (!err) {
          console.log(info.socket);
          // <Socket ...>  (this is a raw net.Socket that is established to the destination host through the given proxy server)
          Socket = info.socket
        } else {
          // Handle errors
        }
      })
    } catch (e) {
      console.log(e)
    }

    return protocol === 'xmpp:'
      ? {port: port ? Number(port) : null, host: hostname}
      : undefined
  }



  // https://xmpp.org/rfcs/rfc6120.html#streams-open
  headerElement() {
    const el = super.headerElement()
    el.name = 'stream:stream'
    el.attrs['xmlns:stream'] = NS_STREAM
    return el
  }

  // https://xmpp.org/rfcs/rfc6120.html#streams-open
  header(el) {
    const frag = el.toString()
    return `<?xml version='1.0'?>` + frag.substr(0, frag.length - 2) + '>'
  }

  // https://xmpp.org/rfcs/rfc6120.html#streams-close
  footer() {
    return '</stream:stream>'
  }
}

ConnectionTCP.prototype.NS = NS_STREAM
ConnectionTCP.prototype.Socket = Socket
ConnectionTCP.prototype.Parser = Parser

module.exports = ConnectionTCP
